# glr

Upload multiple artifacts to Gitlab Tag Release Description.

## Usage

glr [pption] [project id] [tag] [dist dir]

## Options

```
glr \
  -t or --token \ # Gitlab API Access token
  -f or --force \ # Detele if already exist, And create tag and release description
```


## Install
```
go get -u gitlab.com/longkey1/glr.git

```

## Author

[longkey1](hrrps://longkey1.net)
